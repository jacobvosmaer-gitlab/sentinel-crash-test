#!/bin/sh
NREDIS=2
NSENTINEL=3

echo 'This script will terminate all running redis-server and redis-sentinel processes.'
echo 'Enter YES to continue'
read answer
if [ YES != $answer ]; then
  echo Aborting
  exit 1
fi

pkill redis-server
pkill redis-sentinel
sleep 5

for i in $(seq $NREDIS); do
  echo "Booting redis-server $i"
  (cd redis$i && redis-server redis.conf&)
done

for i in $(seq $NSENTINEL); do
  echo "Booting redis-sentinel $i"
  sed "s/5000/500$i/" sentinel.conf > sentinel$i.conf
  redis-sentinel sentinel$i.conf >> sentinel$i.log&
done

./tail-sentinel
